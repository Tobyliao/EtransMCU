/**
 * @file Bluetooth.cpp
 * @brief Serial Communication between MCU and APP
 * @author David D.W.Lee
 * @version 0.1
 * @date 2017-03-12
 */

#include "Bluetooth.h"
#include "GlobalDefine.h"
#include <Arduino.h>

/**
 * @brief Bluetooth initialization.
 */
void InitialBluetooth(void)
{
	Serial.begin(BR);	// Setting Baud Rate
	while (!Serial) {
		; // wait for serial port to connect. Needed for native USB port only
	}
}

/**
 * @brief Sending string through UART.
 *
 * @param inputstring The string you want to send.
 */
void SendString(char* inputstring)
{
	Serial.println(inputstring);
}

/**
 * @brief Receving string untill \n char.
 *
 * @param outputstring Return output string from UART.
 */
void ReceiveString(char* outputstring)
{
	String rcvmsg = Serial.readStringUntil(TERMINAL_CHAR);
	rcvmsg.toCharArray(outputstring, MAX_MSG);
}

/**
 * @brief Decode the signal string
 *
 * @param cmdstring 
 *
 * @return CMD Struct 
 */
CMD DecodeSignal(char* cmdstring)
{
	CMD command;

	switch(cmdstring[0])
	{
		case 'c':
			command.Func_Type = CTRL;
			command.Func_Num = cmdstring[1] - '0';
			break;
		case 's':
			command.Func_Type = SETTING;
			command.Func_Num = cmdstring[1] - '0';
			break;
		default:
			command.Func_Type = UKN;
			command.Func_Num = -1;
	}

	return command;
}

/**
 * @brief Return decode result to APP and current program
 *
 * @param command
 *
 * @return 0 means success ; 1 means fail
 */
int SignalReturn(CMD command)
{
	if(command.Func_Type == UKN){
		SendString(RSENT);
		return 1;
	}
	else{
		SendString(SUCC);
		return 0;
	}
}

/**
 * @brief Handle bluetooth receive, decode, return request
 *
 * @return CMD command Struct
 */
CMD Bluetooth(void)
{
	char cmdString[MAX_MSG];
	CMD command;
	int succOrNot;

	while(Serial.available() <= 0); // waiting for new signal

	if(Serial.available() > 0){
		ReceiveString(cmdString);
		command = DecodeSignal(cmdString);

		if(!SignalReturn(command)) // temporal solution
			return command; // succ
		else
	  		return command; // ukn
	}
}
