/**
 * @file Bluetooth.h
 * @brief Serial Communication between MCU and APP
 * @author David D.W.Lee
 * @version 0.1
 * @date 2017-03-12
 */

#ifndef Bluetooth_h__
#define Bluetooth_h__
#include "GlobalDefine.h"

void InitialBluetooth(void);

void SendString(char* inputstring);
void ReceiveString(char* outputstring);


CMD DecodeSignal(char* cmdstring);
int SignalReturn(CMD command);

CMD Bluetooth(void);

#endif
