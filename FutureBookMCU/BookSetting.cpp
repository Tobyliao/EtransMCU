/**
 * @file BookSetting.cpp
 * @brief Storing for book setting and motor movements
 * @author David D.W.Lee
 * @version 0.1
 * @date 2017-04-25
 */

#include "BookSetting.h"

const int RT_TS = 9;
const float RT_TI[] = {3, 3, 3, 3, 3, 3, 3, 3, 3};
const int RT_EP[][MAXSTEPS] = { //[TOTALMOTORS][MAXSTEPS]
	{ 90,  90,  90,  90,  90,  90,  90,  90,  90},
	{175, 175,  88, 175, 175, 175, 175, 175, 175},
	{ 50,  50,  50,  50,  50,  50,  50,  50,  50},
	{106, 106, 106, 106, 106, 106, 106, 106, 106},
	{124,  95,  95,  95, 124, 124, 124, 124, 124},
	{  0,   0,   0,   0,   0, 110,   0,   0,   0},
	{ 86,  86,  86,  86,  86,  86,  86,  86,  86},
	{151, 151, 151, 151, 151, 151, 151,  12, 150},
	{ 90,  90,  90,  90,  90,  90,  90,  90,  90}
};
