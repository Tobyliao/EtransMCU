/**
 * @file BookSetting.h
 * @brief 
 * @author David D.W.Lee
 * @version 0.1
 * @date 2017-03-27
 */
#ifndef BookSetting_h__
#define BookSetting_h__


#define MAXSTEPS 9


struct BST
{
	int totalSteps;
	float* timeInterval;
	int (*endPosition)[MAXSTEPS];
};

/******** Settings ********/
// Regular Things
extern const int RT_TS;
extern const float RT_TI[];
extern const int RT_EP[][MAXSTEPS];


#endif
