/**
 * @file FutureBookMCU
 * @brief Main section
 * @author David D.W.Lee
 * @version 0.1
 * @date 2017-03-12
 */

#include <Arduino.h>
#include "Bluetooth.h"
#include "ServoMotorControl.h"
#include "GlobalDefine.h"


int MachineStatus; // extern from ServoMotorControl.cpp

Servo servos[TOTALMOTORS];
CMD CurrentCMD;

void setup() {
	MachineStatus = BUSY;
	InitialBluetooth();
 
	MotorInit(servos);
	MachineStatus = IDLE;
}

void loop() {
  Serial.println("Do Action");
	CurrentCMD.Func_Type = WAIT;
	CurrentCMD = Bluetooth();
  Serial.println(CTRL);
  Serial.println(SETTING);
	if(CurrentCMD.Func_Type == CTRL || CurrentCMD.Func_Type == SETTING){
	  MotorManager(CurrentCMD, servos);
    Serial.println("Do Action2");
	  }
   
		
}  
