/**
 * @file GlobalDefine.h
 * @brief Global Definition
 * @author David D.W.Lee
 * @version 0.1
 * @date 2017-03-12
 */

#ifndef GlobalDefine_h__
#define GlobalDefine_h__


/********** Debug **********/

//#define SERVO_BASIC_DEBUG__
//#define BLUETOOTH_BASIC_DEBUG__
//#define BLUETOOTH_RECEIVE_DEBUG__
//#define SERVO_MOVEMENT_DEBUG__




/********** Motors **********/
#define TOTALMOTORS 9	// Total servo motors
#define STDMOTORS 7		// Standard motors
#define CRMOTORS 2		// Continue rotation motors

enum STAT {IDLE, WORKING, BUSY}; // Machine Status
// IDLE: Machine can run command
// WORKING: Machine is running command
// BUSY: Initializing or setting


enum BOOKSET {RT}; // Machine Book Setting









/********** Bluetooth **********/
#define BR 9600
#define TERMINAL_CHAR '\n'

#define MAX_MSG 4


/** Message **/
// Control Signal
#define STOP "c0\n"
#define RESET "c1\n"
#define SCAN_ONE_PAGE "c2\n"

// Setting Signal
// ...
#define RTSIG "s0\n" // Regrlar things?



// Return Signal
#define SUCC "r0\n"
#define RSENT "r1\n" // Receiving undefine signal string
#define RDY "r2\n" // Ready to take photo (i.e. finish one round)
#define STERR "r3\n" // Setting Error
#define CTERR "r4\n" // Control Error
#define ITR "r5\n" // Being interrupt


enum FUNCTYPE {CTRL, SETTING, UKN, WAIT}; // Control Signal, Setting Signal, Unknown Signal, Waitting For Signal

struct CMD
{
	int Func_Type;
	int Func_Num;
};



#endif
