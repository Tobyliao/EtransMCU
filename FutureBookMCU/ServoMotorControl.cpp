/**
 * @file ServoMotorControl.cpp
 * @brief Control Servo Motors
 * @author David D.W.Lee
 * @version 0.1
 * @date 2017-03-19
 */
#include "ServoMotorControl.h"
#include "GLobalDefine.h"
#include "Bluetooth.h" // be used for sending error message
#include <Arduino.h>
#include <ArduinoSTL.h>


// Initial Angle

int ServoInitAngle[]={90,175,50,106,124,0,86,151,90};


BST CurrentBST; // Current Book Setting

extern int MachineStatus; // Current Machine Status

int DefaultBST;

/**
 * @brief Motor Initialization
 *
 * @param servos Defined at the main section. A servo array which need to be inicialized
 */
void MotorInit(Servo* servos)
{
	int i;
	// Initial servos form pin2 (pin1,0 is used for UART)
	for(i=0;i<TOTALMOTORS;i++)
	{
		servos[i].attach(i+2);
	}

	DefaultBST = RT;
	MotorRst(DefaultBST, servos);
}

/**
 * @brief Reset Motors to initial position and set default book setting
 *
 * @param servos
 */
void MotorRst(int rstBST, Servo* servos)
{
  int i;
	// Set initial value
	for(i=0;i<TOTALMOTORS;i++)
	{
		servos[i].write(ServoInitAngle[i]);
	}

	/* Default Settings */
	MachineStatus = IDLE; // Initial Machine Status
	LoadBookSetting(rstBST); // Initial Book Setting
}

/**
 * @brief Executing the entire round of instruction.
 *
 * @param sheet intput BST structure which is saving movements
 * @param servos the initialized servos' pointer
 *
 * @return 0:successful 1:interrupt
 */
int DoOneRound(BST sheet, Servo* servos)
{
	int rnd, serno;
	for(rnd=0;rnd<sheet.totalSteps;rnd++)
	{
		for(serno=0;serno<TOTALMOTORS;serno++)
		{
			if(MachineStatus == WORKING && Serial.available() <= 0){
        
				servos[serno].write(sheet.endPosition[serno][rnd]);

			}
			else{
				return 1; //being interrupted
			}
		}

		delay(sheet.timeInterval[rnd] * 1000);
	}
	return 0; //success
}

/**
 * @brief Loading control type and do the following instruction
 *
 * @param functionType control type
 * @param servos
 */
void MotorControl(int functionType, Servo* servos){
	switch(functionType){
	  // c0~c2
		case 0: // Stop

			MachineStatus = IDLE;

			break;
		case 1: // Reset

			MachineStatus = IDLE;
			MotorRst(DefaultBST, servos);
     
			break;
		case 2: // Flip one page
      Serial.println("Motor CTRL FOP");
			if(!DoOneRound(CurrentBST, servos)){
			  
				SendString(RDY);
			}
			else{
	
				SendString(ITR);	
			}
     
      
			MachineStatus = IDLE;
     
			break;
		default:
			SendString(CTERR);
	}
}

/**
 * @brief Loading Setting Type
 *
 * @param settingType
 */
void MotorSetting(int settingType)
{
	switch(settingType){
		case 0:
			LoadBookSetting(RT);
			break;
		default:
			SendString(STERR);
	}
}

/**
 * @brief Loading BST structure based on setting type
 *
 * @param bookStatus
 */
void LoadBookSetting(int bookStatus)
{

	switch(bookStatus)
	{
		case RT:
			CurrentBST.totalSteps = RT_TS;
			CurrentBST.timeInterval = RT_TI;
			CurrentBST.endPosition = RT_EP;
			break;
		default:
			SendString(STERR);
	}
	
}




/**
 * @brief Handle all the servo motor movement.
 *
 * @param command The command which is decoded by Bluetooth().
 * @param servos The servo array which has been initialized.
 */
void MotorManager(CMD command, Servo* servos)
{
  Serial.println("Manager");
	switch(command.Func_Type){
		case CTRL:
			MachineStatus = WORKING;
			MotorControl(command.Func_Num, servos);
      Serial.println("Manager CTRL");
			break;
		
		case SETTING:
			MachineStatus = BUSY;
			MotorSetting(command.Func_Num);
			MachineStatus = IDLE;
      Serial.println("Manager SETTING");
			break;
	}
}
