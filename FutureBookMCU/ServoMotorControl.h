/*
 *
 * @file ServoMotorControl.h
 * @brief Control Servo Motors
 * @author David D.W.Lee
 * @version 0.1
 * @date 2017-03-19
 */

#ifndef ServoMotorControl_h__
#define ServoMotorControl_h__


#include <Arduino.h>
#include <ArduinoSTL.h>
#include "GlobalDefine.h"
#include <Servo.h>
#include "BookSetting.h"


void MotorInit(Servo* servos);
void MotorRst(int rstBST, Servo* servos);

int DoOneRound(BST sheet, Servo* servos);

void MotorControl(int functionType, Servo* servos);
void MotorSetting(int settingType);

void LoadBookSetting(int bookStatus);


void MotorManager(CMD command, Servo* servos);



#endif
