# FutureBookMCU

## Usage

Work with Arduino UNO


## File Description

* FutureBookMCU.ino : Main File
* GlobalDefine.h : Almost all the global definition
* Bluetooth.cpp(.h) : Receive, Decode, Send any message through UART
* ServoMotorControl.cpp(.h) : Control motor movment depends on the command signal
* BookSetting.cpp(.h) : Store motor movements and corresponding settings

## Autor
**David Lee**
